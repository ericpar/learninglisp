;;; Essais faits dans le cadre de la lecture du livre "Practical
;;; Common Lisp" de Peter Siebel.

(defun hello-world ()
  (format t "Hello world ERIC !"))

(defun add (x y) (+ x y))

# DESCRIPTION #

This repo contains some of the exercices I've run into while reading the book "Practical Common Lisp" from Peter Seibel.

It's incomplete, not necessary well structured and it's meant to be my scratchpad while I learn; I tend to snap a computer whenever I can, wherever I am so having these files available on the could is really a plus for me to learn "on-the-go".